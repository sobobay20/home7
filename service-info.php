<!DOCTYPE html>
<html lang="en">

<?php require_once "requires/head.php"; ?>

<body>
  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center d-flex">
      <a onclick="history.back()" class="logo me-auto">
          <i class="fas fa-chevron-left text-dark"></i>
          <span class="text-black-50 fs-5">Services</span>
      </a>
      <span></span>
      
  
      <a class="me-auto" href="notification.html"> 
        <h6 class="text-center text-theme fs-4 fw-bold">Information</h6>
      </a>
      <nav id="navbar" class="navbar order-first order-lg-0">
        <ul class="nav">
            <li class="nav-item">
            <a class="nav-link" href="news.php">News</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="services.php">Services</a>
            </li>
            <li class="nav-item">
            <a class="nav-link active" href="index.php">Salon</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="allotments.php">Allotments</a>
            </li>
        </ul>
</nav><!-- .navbar -->
 <span class="me-auto"></span>
    </div>
  </header><!-- End Header -->
 <div style="margin-top: 35px !important;"></div>
  <main id="main">
    <section class="mb-4">
      <div class="container">
         <div class="row">
            <div class="col-12 col-lg-2"></div>
            <div class="col-12 col-lg-8">
            <div class="row gy-4">
                <div class="col-lg-4 col-xl-4 col-md-6 col-12">
                  <a href="assets/img/services/salon1.jpg">
                  <img src="assets/img/services/salon1.jpg" class="img img-thumbnail" alt="" >
                  </a>
                </div>
                <div class="col-lg-8 col-xl-8 col-md-6 col-12">
                <span class="fw-bold" >Beauty Palace</span> 
                  <span>
                  <img src="assets/img/tick-mark-blue.png" style="width: 16px; height: 16px;" alt="">
                  </span> <br>
                  <span class="text-dark-75">We serve beauty to all.</span> 
                </div>
              </div> 
            <div class="row gy-4">
         
          <div class="row gy-4 mt-sm-1">
              <div class="col-6 col-md-4">
               <h6 class="mt-2" ><strong style="font-family:Arial, Helvetica, sans-serif;">12</strong> Ratings</h6> 
                <h6>
                    <i class="fas fa-star fa-1xx text-warning"></i>
                    <i class="fas fa-star fa-1xx text-warning"></i>
                    <i class="fas fa-star fa-1xx text-warning"></i>
                    <i class="fas fa-star fa-1xx text-warning"></i>
                    <i class="fas fa-star-half-alt fa-1xx text-warning"></i>
                </h6> 
              </div>
              <div class="col-6 col-md-4">
                <div class="hstack gap-3">
                    <div class="ms-auto">
                    <i class="fas fa-comment-alt text-theme"></i> <br>
                        <span class="fw-bold fs-6">Chat</span>
                    </div>
                    <div class="d-flex" style="height: 40px;">
                    <div class="vr"></div>
                    </div>
                    <div class="me-auto">
                    <i class="fas fa-phone-alt text-theme"></i> <br>
                        <span class="fw-bold fs-6">Call</span>
                    </div>
                </div>
                   
              </div>

              <div class="col-12 col-md-4">
                <div class="hstack">
                    <div class="ms-auto">
                     <button class="btn btn-sm btn-theme">Book Appointment</button>
                    </div>
                    
                </div>
              </div>
            
          </div>
        
          <div class="row gy-4 mt-1">
            <div class="col-12 col-md-12 col-lg-12">
                <h6 class="fw-bold">
                    Photos
                </h6>
                <div id="carouselExampleSlidesOnly" class="carousel slide mt-3" data-bs-ride="carousel">
                    <div class="carousel-inner mr-4">
                        <div class="carousel-item active">
                        <img src="assets/img/services/photos-salon1.jpg" class="d-block img-fluid" alt="image">
                        </div>
                        <div class="carousel-item">
                        <img src="assets/img/services/photos-salon2.jpg" class="d-block img-fluid" alt="image">
                        </div>
                        <div class="carousel-item">
                        <img src="assets/img/services/photos-salon3.jpg" class="d-block img-fluid" alt="image">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-12 col-lg-12">
              <h6 class="fw-bold"><u>About Us</u></h6>
                 <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dorporis laborum asperiores repellendus in magnam facere.</p>
            </div>
          </div>
          <div class="row gy-4 mt-sm-1">
              <div class="col-12 col-md-12 col-lg-6">
              <h6 class="fw-bold"><u>Scope</u></h6>
              <span class="mr-1"><i class="fas fa-check text-theme"></i></span><span>Kotei</span> <br>
              <span class="mr-1"><i class="fas fa-check text-theme"></i></span><span>Ayeduase New Site</span> <br>
              <span class="mr-1"><i class="fas fa-check text-theme"></i></span><span>Tech</span> <br>
              <span class="mr-1"><i class="fas fa-check text-theme"></i></span><span>Bomso</span>
              </div>
              <div class="col-12 col-md-12 col-lg-6">
              <h6 class="fw-bold"><u>Working Days</u></h6>
              <div class="hstack gap-3 bx-pull-left">
                    <div class="ms-auto">
                   <span>Mondays - Fridays</span>
                    </div>
                    <div class="d-flex" style="height: 20px;">
                    <div class="vr"></div>
                    </div>
                    <div class="me-auto">
                        <span>9am - 5pm</span>
                    </div>
                </div>
                
              </div>
              <div class="col-12 col-md-12 col-lg-6">
              <h6 class="fw-bold"><u>Conditions/Requirements</u></h6>
                 <span class="mr-1"><i class="fas fa-check text-theme"></i></span><span>Availabilty of electricity.</span> <br>
                 <span class="mr-1"><i class="fas fa-check text-theme"></i></span><span>Availabilty of water.</span> <br>
              </div>
          </div>
       
            </div>
            <div class="col-12 col-lg-2"></div>
         </div>
      </div>
    </section>

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
 
  <?php require_once "requires/footer.php"; ?>
  <div id="preloader"></div>

  <?php require_once "requires/scripts.php"; ?>

</body>

</html>