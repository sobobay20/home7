<!DOCTYPE html>
<html lang="en">

<?php require_once "requires/head.php"; ?>

<body>
  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center d-flex">
      <a href="index.php" class="logo me-auto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>
      <span></span>
      
  
      <a class="me-auto" href="notification.html"> 
        <h5 class="text-center text-black-50">Allotments</h5>
      </a>

      <nav id="navbar" class="navbar order-first order-lg-0">
        <ul class="nav">
            <li class="nav-item">
            <a class="nav-link" href="news.php">News</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="services.php">Services</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="index.php">Salon</a>
            </li>
            <li class="nav-item">
            <a class="nav-link active" href="allotments.php">Allotments</a>
            </li>
        </ul>
</nav><!-- .navbar -->

      <?php require_once "requires/nav.php"; ?>


    </div>
  </header><!-- End Header -->
 <div style="margin-top: 30px !important;"></div>
  <main id="main">
  <section>
      <div class="container">
      <div class="row">
          <div class="col-12 col-lg-3"></div>
          <div class="col-12 col-lg-6">
          <div class="row gy-4">
                <div class="col-lg-6 col-md-12 col-4">
                <img src="assets/img/services/salon5.jpg" class="img img-thumbnail shadow-sm " alt="" >
                </div>
                <div class="col-lg-6 col-md-12 col-8">
                    <h6 class="cursor text-black-50 fw-bold" >Salon E</h6>
                    <span>Kotei, Kumasi</span>   
                </div> 
                <div class="actions">
                  <span class="inline-block text-dark cursor" style="font-size:12px;">Track Order</span>
                  <span class="inline-block"><i class="fas fa-arrow-right fa-sm"></i></span>
                  <span class="inline-block mr-1"><button class="btn btn-sm btn-theme">Cancel</button></span>
                  <span class="inline-block"><button class="btn btn-sm btn-theme">Reschedule</button></span>
                </div>    
              </div>
            <div class="row gy-4">
              <div class="col-12">
               <div class="header-1">
                <img src="assets/img/services/salon1.jpg" class="img img-thumbnail shadow-sm" />
                <span><h5>Beauty Palour</h5></span>

                <div class="actions">
                  <span class="inline-block text-dark cursor" style="font-size:12px;">Track Order</span>
                  <span class="inline-block"><i class="fas fa-arrow-right fa-sm"></i></span>
                  <span class="inline-block mr-1"><button class="btn btn-sm btn-theme">Cancel</button></span>
                  <span class="inline-block"><button class="btn btn-sm btn-theme">Reschedule</button></span>
                </div>
               </div>             
              </div>       
            </div>

           
           
          </div>
          <div class="col-12 col-lg-3"></div>
      </div>    
      </div>
    </section>

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <?php require_once "requires/footer.php"; ?>
  <div id="preloader"></div>
  <?php require_once "requires/scripts.php"; ?>

</body>

</html>

