<footer id="footer">

    <div class="container d-md-flex py-4 justify-content-center">
      <nav class="nav__">
        <a href="news.php" class="nav__link active">
          <i class="fas fa-newspaper text-white fa-2x"></i>
          <!-- <img style="width: 30px; height: 30px;" src="assets/img/footer-icons/news.png"/> -->
          <span class="nav__text">News</span>
        </a>
        <a href="services.php" class="nav__link">
        <i class="fas fa-wrench text-white fa-2x"></i>
          <!-- <img style="width: 30px; height: 30px;" src="assets/img/footer-icons/services.png"/> -->
          <span class="nav__text">Services</span>
        </a>
        <a href="index.php" class="nav__link">
        <i class="fas fa-store text-white fa-2x"></i>
          <!-- <img style="width: 30px; height: 30px;" src="assets/img/footer-icons/salon.png"/> -->
          <span class="nav__text">Salon</span>
        </a>
        <a href="allotments.php" class="nav__link">
        <i class="fas fa-calendar text-white fa-2x"></i>
          <!-- <img style="width: 30px; height: 30px;" src="assets/img/footer-icons/allotments.png"/> -->
          <span class="nav__text">Allotments</span>
        </a>
       
      </nav>
    </div>
  </footer><!-- End Footer -->
