<!DOCTYPE html>
<html lang="en">

<?php require_once "requires/head.php"; ?>

<body>
  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center d-flex">
      <a href="index.php" class="logo me-auto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>
      <span></span>
      
  
      <a class="me-auto" href="notification.html"> 
        <h5 class="text-center text-theme fs-4 fw-bold">News</h5>
      </a>

      <nav id="navbar" class="navbar order-first order-lg-0">
        <ul class="nav">
            <li class="nav-item">
            <a class="nav-link active" href="news.php">News</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="services.php">Services</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="index.php">Salon</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="allotments.php">Allotments</a>
            </li>
        </ul>
</nav><!-- .navbar -->

      <?php require_once "requires/nav.php"; ?>

    </div>
  </header><!-- End Header -->
 <div style="margin-top: 30px !important;"></div>
  <main id="main">
    <section>
      <div class="container">
      <div class="row">
          <div class="col-12 col-lg-2"></div>
          <div class="col-12 col-lg-8">
            <div class="row gy-4">
              <div class="col-12">
                <div class="header">
                  <img src="assets/img/logo.png" style="border-radius:50%; width: 70px; height: 70px" alt="logo" />
                  <span><h5>Home 7</h5> </span>
                  <span class="text-muted mt-n1">October 21,2022 @ 12:00pm</span> 
                  <i class="fas fa-ellipsis-v"></i> 
               </div>  
               <div>
               <img src="assets/img/services/salon1.jpg" class="img-fluid shadow-sm br-1" />
               <h5 class="mt-2">Flyer Title</h5>
               <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
               </div>  
              </div>       
            </div>
            <hr>
            <div class="row gy-4">
              <div class="col-12">
                <div class="header">
                  <img src="assets/img/logo.png" style="border-radius:50%; width: 70px; height: 70px" alt="logo" />
                  <span><h5>Home 7</h5> </span>
                  <span class="text-muted mt-n1">October 21,2022 @ 12:00pm</span> 
                  <i class="fas fa-ellipsis-v"></i> 
               </div>  
               <div>
               <img src="assets/img/services/salon1.jpg" class="img-fluid shadow-sm br-1" />
               <h5 class="mt-2">Flyer Title</h5>
               <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
               </div>  
              </div>       
            </div>
            <hr>
            <div class="row gy-4">
              <div class="col-12">
                <div class="header">
                  <img src="assets/img/logo.png" style="border-radius:50%; width: 70px; height: 70px" alt="logo" />
                  <span><h5>Home 7</h5> </span>
                  <span class="text-muted mt-n1">October 21,2022 @ 12:00pm</span> 
                  <i class="fas fa-ellipsis-v"></i> 
               </div>  
               <div>
               <img src="assets/img/services/salon1.jpg" class="img-fluid shadow-sm br-1" />
               <h5 class="mt-2">Flyer Title</h5>
               <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
               </div>  
              </div>       
            </div>
          </div>
          <div class="col-12 col-lg-2"></div>
      </div>    
      </div>
    </section>

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <?php require_once "requires/footer.php"; ?>

  <div id="preloader"></div>

  <?php require_once "requires/scripts.php"; ?>

</body>

</html>