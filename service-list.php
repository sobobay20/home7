<!DOCTYPE html>
<html lang="en">

<?php require_once "requires/head.php"; ?>

<body>
  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center d-flex">
      <a href="index.php" class="logo me-auto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>
      <span></span>
      
  
      <a class="me-auto" href="notification.html"> 
        <h6 class="text-center text-theme fs-4 fw-bold">Carpenters</h6>
      </a>
      <nav id="navbar" class="navbar order-first order-lg-0">
        <ul class="nav">
            <li class="nav-item">
            <a class="nav-link" href="news.php">News</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="services.php">Services</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="index.php">Salon</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="allotments.php">Allotments</a>
            </li>
        </ul>
</nav><!-- .navbar -->
      <?php require_once "requires/nav.php"; ?>

     

    </div>
  </header><!-- End Header -->
 <div style="margin-top: 35px !important;"></div>
  <main id="main">
    <section class="">
      <div class="container">
        <div class="row">
            <div class="col-12 col-lg-2"></div>
            <div class="col-12 col-lg-8">
            <div class="row gy-4 mb-3">
            <div class="col-12">
            <div class="form-group has-search">
                <span class="fa fa-search form-control-feedback"></span>
                <input style="border-radius: 20px;" type="search" class="form-control" placeholder="Search for Carpenters">
            </div>
            </div>
        </div>
        <a href="service-info.php">
            <div class="card card-custom mt-2 shadow">
            <div class="card-body">
              <div class="row gy-4">
                <div class="col-lg-12 col-md-12 col-12">
                      <span class="cursor text-black-50 fw-bold" >Carpenter A</span>
                        <span>
                         <img class="mt-n1" src="assets/img/tick-mark-blue.png" style="width: 16px; height: 16px;" alt="">
                        </span> <br>
                      <span class="mb-1">Kotei, Kumasi</span> 
                    <img src="assets/img/services/salon1.jpg" class="img-fluid img-thumbnail" />
                </div>
              </div> 
           
            </div>
            </div>   
        </a>

        <a href="service-info.php">
            <div class="card card-custom mt-2 shadow">
            <div class="card-body">
            <div class="row gy-4">
                <div class="col-lg-6 col-md-12 col-4">
                <img src="assets/img/departments-1.jpg" class="img-fluid img-thumbnail" alt="" >
                </div>
                <div class="col-lg-6 col-md-12 col-8">
                    <span class="cursor text-black-50 fw-bold" >Carpenter B</span>
                     <span>
                        <img class="mt-n1" src="assets/img/tick-mark-blue.png" style="width: 16px; height: 16px;" alt="">
                    </span> <br>
                    <span>Kotei, Kumasi</span>  
                </div>     
            </div>
            </div>
            </div>   
        </a>

        <a href="service-info.php">
            <div class="card card-custom mt-2 shadow">
            <div class="card-body">
            <div class="row gy-4">
                <div class="col-lg-6 col-md-12 col-4">
                <img src="assets/img/departments-1.jpg" class="img-fluid img-thumbnail" alt="" >
                </div>
                <div class="col-lg-6 col-md-12 col-8">
                    <span class="cursor text-black-50 fw-bold" >Carpenter C</span> <br>
                    <span>Kotei, Kumasi</span>  
                </div>     
            </div>
            </div>
            </div>   
        </a>

        <a href="service-info.php">
            <div class="card card-custom mt-2 shadow">
            <div class="card-body">
            <div class="row gy-4">
                <div class="col-lg-6 col-md-12 col-4">
                <img src="assets/img/departments-1.jpg" class="img-fluid img-thumbnail" alt="" >
                </div>
                <div class="col-lg-6 col-md-12 col-8">
                    <span class="cursor text-black-50 fw-bold" >Carpenter D</span> <br>
                    <span>Kotei, Kumasi</span>  
                </div>     
            </div>
            </div>
            </div>   
        </a>

        <a href="service-info.php">
            <div class="card card-custom mt-2 shadow">
            <div class="card-body">
            <div class="row gy-4">
                <div class="col-lg-6 col-md-12 col-4">
                <img src="assets/img/departments-1.jpg" class="img-fluid img-thumbnail" alt="" >
                </div>
                <div class="col-lg-6 col-md-12 col-8">
                    <span class="cursor text-black-50 fw-bold" >Carpenter E</span> <br>
                    <span>Kotei, Kumasi</span>  
                </div>     
            </div>
            </div>
            </div>   
        </a>
            </div>
            <div class="col-12 col-lg-2"></div>
        </div>            
      </div>
    </section>

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
 
  <?php require_once "requires/footer.php"; ?>
  <div id="preloader"></div>

  <?php require_once "requires/scripts.php"; ?>

</body>

</html>